Activity 30

Objective 1
db.fruits.aggregate([
     { $match: {onSale: true}},
     { $count: "fruitsOnSale"},
     { $out:"fruitOnSale"}
    ]);

Objective 2
db.fruits.aggregate([
     { $match: {stocks: {$gte: 20}}},
     { $count: "enoughStock"},
     { $out:"enoughStocks"}
    ]);

Objective 3
db.fruits.aggregate([
     { $match: {onSale: true}},
     { $group: { _id:"$supplier_id", avg_price:{$avg:"$price"}}},
     { $out:"avgSalePrices"}
    ]);


Objective 4
db.fruits.aggregate([
     { $match: {onSale: true}},
     { $group: { _id:"$supplier_id", max_price:{$max:"$price"}}},
     { $out:"highestPriced"}
    ]);

Objective 5

db.fruits.aggregate([
     { $match: {onSale: true}},
     { $group: { _id:"$supplier_id", max_price:{$min:"$price"}}},
     { $out:"lowestPriced"}
    ]);